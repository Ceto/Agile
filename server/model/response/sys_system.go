package response

import "Agile/config"

type SysConfigResponse struct {
	Config config.Server `json:"config"`
}
