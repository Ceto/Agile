package response

import "Agile/model"

type ExaFileResponse struct {
	File model.ExaFileUploadAndDownload `json:"file"`
}
