package response

import "Agile/model"

type ExaCustomerResponse struct {
	Customer model.ExaCustomer `json:"customer"`
}
