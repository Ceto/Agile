package request

import "Agile/model"

type SysOperationRecordSearch struct {
	model.SysOperationRecord
	PageInfo
}
