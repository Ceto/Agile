package request

import "Agile/model"

type {{.StructName}}Search struct{
    model.{{.StructName}}
    PageInfo
}